from Activity import selector
from RAS import RAS
import unittest

class TestCaseMethod(unittest.TestCase):
    def test_selector(self):
        s = [1,3,0,5,3,5,6,8,8,2,12]
        f = [4,5,6,7,9,9,10,11,12,14,16]
        self.assertListEqual(selector(s,f), [0,3,7,10])
        
    def test_RAS(self):
        s = [1,3,0,5,3,5,6,8,8,2,12]
        f = [4,5,6,7,9,9,10,11,12,14,16]
        self.assertListEqual(RAS(s,f,-1,11), [0,3,7,10])
        
if __name__ == "__main__":
    unittest.main()