A = []
def RAS(s,f,k,n):
    m = k+1
    while(m < n and s[m] < f[k] and k >= 0):
        m = m + 1

    if(m < n):
        A.append(m)
        RAS(s,f,m,n)
    else:
        return 0
    return A